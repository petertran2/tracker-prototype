class ApplicationController < ActionController::Base

	helper_method :current_user
	
	def log_in_user! user
		@current_user = user
		session[:session_token] = user.reset_session_token!
		redirect_to user_url(user)
	end
	
	def current_user
		return nil if session[:session_token].nil?
		@current_user ||= User.find_by(session_token: session[:session_token])
	end
	
	def logged_in?
		!current_user.nil?
	end
	
	def not_logged_in
		redirect_to new_session_url if !logged_in?
	end
	
end
