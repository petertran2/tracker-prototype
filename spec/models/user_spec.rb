require 'rails_helper'

RSpec.describe User, type: :model do

	let(:user) { User.new(email: 'test@email.com', password: '123') }
  
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_length_of(:password).is_at_least(1).on(:create) }
  
  describe '#is_password?' do
  	it 'verifies a password is the correct one' do
  		expect(user.is_password? '123').to be true
  	end
  	it 'verifies a password is not the correct one' do
  		expect(user.is_password? 'abc').to be false
  	end
  end
  
  describe '#reset_session_token!' do
  	it 'resets session token' do
  		old_token = user.session_token
  		new_token = user.reset_session_token!
  		expect(new_token).to_not eq(old_token)
  	end
  end
  
  describe ".find_by_credentials" do
		before(:each) { user.save! }
    it "returns user given correct email and password" do
      expect(User.find_by_credentials("test@email.com", "123")).to eq(user)
    end
    it 'returns nil given incorrect email' do
    	expect(User.find_by_credentials("fail@case.com", "123")).to eq(nil)
    end
    it "returns nil given incorrect password" do
      expect(User.find_by_credentials("test@email.com", "abc")).to eq(nil)
    end
  end
end
