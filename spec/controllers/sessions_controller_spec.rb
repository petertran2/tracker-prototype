require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe "GET #new" do
    it "renders the new template" do
      get :new, {}
      expect(response).to render_template("new")
    end
  end
  
  let(:a_user) { User.create(email: 'test@email.com', password: '123') }

  describe "POST #create" do
    context "with invalid params" do
      it "validates the presence of the user's email and password" do
      	post :create, params: { user: { email: 'test@email.com' } }
				expect(response).to render_template(:new)
#				expect(flash[:errors]).to be_present
      end
      
      it 'rejects non-existent accounts' do
      	post :create, params: { user: { email: '123@go.com', password: '123' } }
				expect(response).to render_template(:new)
#				expect(flash[:errors]).to be_present
      end
    end

    context "with valid params" do
      it "redirects user to new_session_url on success" do
      	post :create, params: { user: { email: 'test@email.com', password: '123'
      		} }
      	expect(response).to redirect_to(new_session_url)
      end
    end
  end

end
